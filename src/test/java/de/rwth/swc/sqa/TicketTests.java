package de.rwth.swc.sqa;

import de.rwth.swc.sqa.model.Ticket;
import io.restassured.RestAssured;
import io.restassured.response.ExtractableResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;
import static io.restassured.RestAssured.with;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class TicketTests {
    private static final String TICKET_PATH = "/tickets";
    private static final String TICKET_VALIDATE_PATH = "/tickets/validate";

    private static final String CUSTOMER_PATH = "/customers";


    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    public void whenBuyTicket_thenValuesSet() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1996-01-01\", \"validFor\" : \"30d\", \"discountCard\" : 25, \"student\" : true, \"disabled\" : false }";
        ExtractableResponse res = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract();
        assertNotNull(res.path("id"));
        assertEquals(res.path("validFrom"), "2020-10-29T10:38:59");
        assertEquals(res.path("birthdate"), "1996-01-01");
        assertEquals(res.path("validFor"), "30d");
        assertEquals(res.path("disabled"), false);
        assertEquals(res.path("discountCard"), (Integer) 25);
        assertNull(res.path("zone"));
        assertEquals(res.path("student"), true);
    }

    @Test
    public void whenBuyTicket_withoutDisabled_thenDisabledFalse() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1996-01-01\", \"validFor\" : \"30d\", \"discountCard\" : 25, \"student\" : true }";
        boolean disabled = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("disabled");
        assertFalse(disabled);
    }

    @Test
    public void whenBuyTicket_withoutZone_thenZoneNull() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1996-01-01\", \"validFor\" : \"30d\", \"discountCard\" : 25, \"student\" : true }";
        Ticket.ZoneEnum zone = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("zone");
        assertNull(zone);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"B\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1992-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
           "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1992-01-01\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"30d\", \"disabled\" : true, \"discountCard\" : 25, \"student\" : true }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : 50, \"student\" : true }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : 25, \"student\" : true }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\",\"discountCard\" : 25, \"student\" : true }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"discountCard\" : 25 }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"discountCard\" : 50 }"
    })
    void whenBuyTicket_thenCreated(String input) {
        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201);
    }

    @Test
    public void whenBuyTicket_withoutDiscountCard_thenDiscountCardFalse() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2002-01-01\", \"validFor\" : \"1h\", \"disabled\" : false, \"zone\" : \"C\", \"student\" : false }";
        Integer discountCard = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("discountCard");
        assertNull(discountCard);
    }

    @Test
    public void whenBuyTicket_withoutStudent_thenStudentFalse() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1992-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\" }";
        boolean student = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("student");
        assertFalse(student);
    }

    @Test
    public void whenBuyTicket_thenValidFromIsSet() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2002-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }";
        String validFrom = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("validFrom");
        assertEquals("2020-10-29T10:38:59", validFrom);
    }

    @Test
    public void whenBuyTicket_thenBirthdateIsSet() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2002-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }";
        String birthdate = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("birthdate");
        assertEquals("2002-01-01", birthdate);
    }

    @Test
    public void whenBuyTicket_thenValidForIsSet() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2002-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }";
        String validFor = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("validFor");
        assertEquals("1h", validFor);
    }

    @Test
    public void whenBuyTicket_thenZoneIsSet() {
        String body = "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2002-01-01\", \"validFor\" : \"1h\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }";
        String zone = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201)
                .extract().path("zone");
        assertEquals("A", zone);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"B\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"30d\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"30d\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"B\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"B\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : true }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1951-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2201-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2021-01-01\", \"validFor\" : \"1d\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : true }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : 25, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : true, \"discountCard\" : 50, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"1959-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : true }"
    })
    void whenBuyTicket_invalidTicketRequest_then400(String input) {
        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(400);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"validFrom\" : \"2020-13-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-13-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2002-10-29T10:38:59\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"invalid value\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"invalid value\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"10y\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"C\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"D\", \"student\" : false }",
            "{\"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"D\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"D\", \"student\" : false }",
            "{ \"validFrom\" : \"2020-10-29T10:38:59\", \"birthdate\" : \"2001-01-01\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"D\", \"student\" : false }",
            "{ }"
    })
    void whenBuyTicket_invalidValues_then400(String input) {
        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(400);
    }


    // VALIDATE TICKET TEST
    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"B\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\" }"
    })
    void whenValidateTicket_thenOk(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"B\", \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add consumer and discount card
        String discountCardJSON =  "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}";
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        discountCardJSON = discountCardJSON.replace("<customerID>", customerId.toString());

        Long discountCardID = with().header("Content-type", "application/json").body(discountCardJSON)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201).extract().path("id");

        input = input.replace("<discountID>", discountCardID.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(200);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : true, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"B\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID> }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"C\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"discountCardId\" : <discountID>}"
    })
    void whenValidateTicket_withDiscountCard_thenOk(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : 25, \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add consumer and discount card
        String discountCardJSON =  "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"2020-01-01\", \"validFor\": \"1y\"}";
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        discountCardJSON = discountCardJSON.replace("<customerID>", customerId.toString());

        Long discountCardID = with().header("Content-type", "application/json").body(discountCardJSON)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201).extract().path("id");

        input = input.replace("<discountID>", discountCardID.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(200);
    }

    @Test
    void whenValidateTicket_ticketNotExisting_then403() {
        String request = "{ \"ticketId\" : 234, \"zone\" : \"C\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"student\" : false }";

        with().header("Content-type", "application/json").body(request)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"B\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"C\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"student\" : false }",
    })
    void whenValidateTicket_invalidZone_then403(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : false, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add costumer
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201);

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2030-10-29T10:28:59\", \"disabled\" : false, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:28:59\", \"disabled\" : false, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T00:00:00\", \"disabled\" : false, \"student\" : false }"
    })
    void whenValidateTicket_invalidDate_then403(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-28T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1d\", \"disabled\" : false, \"discountCard\" : null, \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add costumer
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201);

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : true, \"discountCardId\" : <discountID>, \"student\" : false }",
    })
    void whenValidateTicket_invalidDiscountCardDate_then403(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : 50, \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add consumer and discount card
        String discountCardJSON =  "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"1y\"}";
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        discountCardJSON = discountCardJSON.replace("<customerID>", customerId.toString());

        Long discountCardID = with().header("Content-type", "application/json").body(discountCardJSON)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201).extract().path("id");

        input = input.replace("<discountID>", discountCardID.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }

    @Test
    void whenValidateTicket_invalidDisabledState_then403() {
        String input = "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2030-10-29T10:38:59\", \"disabled\" : false, \"student\" : false }";
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1h\", \"disabled\" : true, \"discountCard\" : null, \"zone\" : \"A\", \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add costumer
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201);

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }

    // TODO: reevaluate
    // also mismatch of disabled, discountCard in ticketValidationRequest not in ticket
    // specification and forum posts by Nils Wild are contradictory: https://moodle.rwth-aachen.de/mod/forum/discuss.php?d=160091
    /*
    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"C\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"student\" : <1> }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"C\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"student\" : <2> }",
    })
    void whenValidateTicket_nonMatchingStudentProperty_then403(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:38:58\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : null, \"student\" : <3> }";
        if(input.contains("<1>")) {
            input = input.replace("<1>", "true");
            addTicketJSON = addTicketJSON.replace("<3>", "false");
        } else {
            input = input.replace("<2>", "false");
            addTicketJSON = addTicketJSON.replace("<3>", "true");
        }

        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }
    */

    @Test
    void whenValidateTicket_invalidDiscountID_then403() {
        String input = "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }";
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : 25, \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add consumer
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201);

        Long discountCardID = 10L;
        input = input.replace("<discountID>", discountCardID.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(403);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"invalid time\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-10-29\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020-13-29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }",
            "{ \"ticketId\" : <ticketID>, \"zone\" : \"A\", \"date\" : \"2020/10/29T10:38:59\", \"disabled\" : false, \"discountCardId\" : <discountID>, \"student\" : false }"
    })
    void whenValidateTicket_invalidSyntax_then400(String input) {
        // add ticket
        String addTicketJSON =  "{ \"validFrom\" : \"2020-10-29T10:28:59\", \"birthdate\" : \"2001-01-01\", \"validFor\" : \"1y\", \"disabled\" : false, \"discountCard\" : 50, \"student\" : false }";
        Long ticketID = with().header("Content-type", "application/json").body(addTicketJSON)
                .when()
                .request("POST", TICKET_PATH)
                .then()
                .statusCode(201).extract().path("id");
        input = input.replace("<ticketID>", ticketID.toString());

        //add consumer and discount card
        String discountCardJSON =  "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"2020-01-01\", \"validFor\": \"1y\"}";
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        discountCardJSON = discountCardJSON.replace("<customerID>", customerId.toString());

        Long discountCardID = with().header("Content-type", "application/json").body(discountCardJSON)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201).extract().path("id");

        input = input.replace("<discountID>", discountCardID.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", TICKET_VALIDATE_PATH)
                .then()
                .statusCode(400);
    }

}
