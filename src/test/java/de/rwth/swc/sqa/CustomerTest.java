package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static io.restassured.RestAssured.with;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CustomerTest {
    private static final String CUSTOMER_PATH = "/customers";

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }",
            "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : false }",
            "{ \"birthdate\" : \"1992-01-01\"}",
    })
    void whenAddCustomer_thenCreated(String input) {
        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201);
    }

    @Test
    public void whenAddCustomer_withoutDisabled_thenDisabledFalse() {
        String body = "{ \"birthdate\" : \"1992-01-01\"}";
        boolean disabled = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201)
                .extract().path("disabled");
        assertFalse(disabled);
    }
    @Test
    public void whenAddCustomer_thenBirthdateIsSet() {
        String body = "{ \"birthdate\" : \"1992-01-01\", \"disabled\": false }";
        String birthdate = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201)
                .extract().path("birthdate");
        assertEquals("1992-01-01", birthdate);
    }
    @Test
    public void whenAddCustomer_thenIDIsSet() {
        String body = "{ \"birthdate\" : \"1992-01-01\", \"disabled\": false }";
        Long id = with().header("Content-type", "application/json")
                .body(body)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201)
                .extract().path("id");
        assertNotEquals(null, id);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "",
            "invalid syntax",
            "{ }"
    })
    void whenAddCustomer_badBodySyntax_thenBadRequest(String input) {
        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(400);
    }
    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"birthdate\" : null, \"disabled\" : true }",
            "{ \"birthdate\" : \"1992-13-01\", \"disabled\" : true }",
            "{ \"birthdate\" : \"2020-10-29T10:38:59\", \"disabled\" : true }",
            "{ \"id\": 3, \"birthdate\" : null, \"disabled\" : true }",
            "{ \"disabled\" : true }"
    })
    void whenAddCustomer_invalidBodyArgs_thenBadRequest(String input) {
        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(400);
    }

    /*
    @Test
    public void whenAddCustomer_wrongData_thenBadRequest() throws Exception {
        String body = "{ \"birthdate\" : null, \"disabled\" : true }";
        with().header("Content-type", "application/json").body(body)
        .when()
        .request("POST", PATH)
        .then()
        .statusCode(400);
    }

    @Test
    public void whenAddCustomer_EmptyBody_thenBadRequest() throws Exception {
        String body = "";
        with().header("Content-type", "application/json").body(body)
        .when()
        .request("POST", PATH)
        .then()
        .statusCode(400);
    }

    @Test
    public void whenAddCustomer_IdInBody_thenBadRequest() throws Exception {
        String body = "{\"id\" : 3, \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        with().header("Content-type", "application/json").body(body)
        .when()
        .request("POST", PATH)
        .then()
        .statusCode(400);
    }

     */

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID>, \"type\": 50, \"validFrom\": \"1993-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-10-01\", \"validFor\": \"1y\"}",
    })
    void whenAddDiscountCardToCustomer_thenCreated(String input) {
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        input = input.replace("<customerID>", customerId.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{ \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID>,\"validFrom\": \"1993-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFor\": \"1y\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\"}",
            "{ }"
    })
    void whenAddDiscountCardToCustomer_incompleteInput_then400(String input) {
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        input = input.replace("<customerID>", customerId.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", path)
                .then()
                .statusCode(400);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{\"id\": 343, \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID2>, \"type\": 25, \"validFrom\": \"1993-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID>, \"type\": 30, \"validFrom\": \"1993-01-01\", \"validFor\": \"30d\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"2020-10-29T10:38:59\", \"validFor\": \"1y\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"null\", \"validFor\": \"1y\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"2020-10-29\", \"validFor\": \"1d\"}",
            "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"2020-10-29\", \"validFor\": \"null\"}"
    })
    void whenAddDiscountCardToCustomer_invalidInput_then400(String input) {
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        input = input.replace("<customerID>", customerId.toString());
        Long customerId2 = customerId+1;
        input = input.replace("<customerID2>", customerId2.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", path)
                .then()
                .statusCode(400);
    }

    @Test
    public void whenAddDiscountCardToCustomer_invalidCustomer_then404() {
        String input = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}";
        Long customerId = 32894L;
        String path = "/customers/" + customerId + "/discountcards";
        input = input.replace("<customerID>", customerId.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", path)
                .then()
                .statusCode(404);
    }
    @Test
    public void whenAddDiscountCardToCustomer_timeConflict_then409() {
        String input1 = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-06-01\", \"validFor\": \"30d\"}";
        String input2 = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"1y\"}";

        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        input1 = input1.replace("<customerID>", customerId.toString());
        input2 = input2.replace("<customerID>", customerId.toString());

        with().header("Content-type", "application/json").body(input1)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201);
        with().header("Content-type", "application/json").body(input2)
                .when()
                .request("POST", path)
                .then()
                .statusCode(409);
    }

    @Test
    public void whenAddDiscountCardToCustomer_noTimeConflict_then201() {
        String input1 = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-02-01\", \"validFor\": \"1y\"}";
        String input2 = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}";

        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        String path = "/customers/" + customerId.toString() + "/discountcards";
        input1 = input1.replace("<customerID>", customerId.toString());
        input2 = input2.replace("<customerID>", customerId.toString());

        with().header("Content-type", "application/json").body(input1)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201);
        with().header("Content-type", "application/json").body(input2)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201);
    }

    @Test
    public void whenGetCustomerDiscountCards_then200() {
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");
        Long customerId2 = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");

        String path = "/customers/" + customerId.toString() + "/discountcards";
        String path2 = "/customers/" + customerId2.toString() + "/discountcards";


        String input = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}";
        String input2 = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1996-01-01\", \"validFor\": \"30d\"}";
        String input3 = "{ \"customerId\": <customerID>, \"type\": 25, \"validFrom\": \"1992-01-01\", \"validFor\": \"30d\"}";
        input = input.replace("<customerID>", customerId.toString());
        input2 = input2.replace("<customerID>", customerId.toString());
        input3 = input3.replace("<customerID>", customerId2.toString());

        with().header("Content-type", "application/json").body(input)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201);
        with().header("Content-type", "application/json").body(input2)
                .when()
                .request("POST", path)
                .then()
                .statusCode(201);
        with().header("Content-type", "application/json").body(input3)
                .when()
                .request("POST", path2)
                .then()
                .statusCode(201);

        ArrayList<String> discountCards1 = with().header("Content-type", "application/json")
                .when()
                .request("GET", path)
                .then()
                .statusCode(200)
                .extract().path("");
        assertEquals(discountCards1.size(), 2);
        ArrayList<String> discountCards2 = with().header("Content-type", "application/json")
                .when()
                .request("GET", path2)
                .then()
                .statusCode(200)
                .extract().path("");
        assertEquals(discountCards2.size(), 1);
    }

    @Test
    public void whenGetCustomerDiscountCards_invalidID_then400() {
        String path = "/customers/" + "/discountcards";

        with().header("Content-type", "application/json")
                .when()
                .request("GET", path)
                .then()
                .statusCode(400);
    }

    @Test
    public void whenGetCustomerDiscountCards_noCustomer_then404() {
        String path = "/customers/" + 1234 + "/discountcards";

        with().header("Content-type", "application/json")
                .when()
                .request("GET", path)
                .then()
                .statusCode(404);
    }

    @Test
    public void whenGetCustomerDiscountCards_noDiscountCardFound_then404() {
        String createCustomerStr = "{ \"birthdate\" : \"1992-01-01\", \"disabled\" : true }";
        Long customerId = with().header("Content-type", "application/json").body(createCustomerStr)
                .when()
                .request("POST", CUSTOMER_PATH)
                .then()
                .statusCode(201).extract().path("id");

        String path = "/customers/" + customerId.toString() + "/discountcards";

        with().header("Content-type", "application/json")
                .when()
                .request("GET", path)
                .then()
                .statusCode(404);
    }
}
