package de.rwth.swc.sqa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {


        SpringApplication.run(Application.class, args);
    }

    public static Map<Long, de.rwth.swc.sqa.model.Customer> customerTable = new HashMap<>();
    public static Map<Long, de.rwth.swc.sqa.model.DiscountCard> discountCardTable = new HashMap<>();

    public static Map<Long, de.rwth.swc.sqa.model.Ticket> tickets = new HashMap<>();

}
