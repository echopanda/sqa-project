package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.Application;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalUnit;
import java.util.Objects;
import java.util.Random;


@Controller
public class TicketController implements de.rwth.swc.sqa.api.TicketsApi {
    public Boolean isValidDateTime(String date) {
        try {
            LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }
    public Boolean isValidDate(String date) {
        try {
            LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    public static LocalDateTime getDateTime(String date) {
        return LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }
    public static LocalDate getDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
    }


    public ResponseEntity<de.rwth.swc.sqa.model.Ticket> buyTicket(de.rwth.swc.sqa.model.TicketRequest body) {
        Ticket ticket = new Ticket();

        if (body.getValidFrom() == null || !isValidDateTime(body.getValidFrom()) ||
                body.getBirthdate() == null || !isValidDate(body.getBirthdate())) {
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
        ticket.setValidFrom(body.getValidFrom());
        ticket.setValidFor(Ticket.ValidForEnum.fromValue(body.getValidFor().toString()));

        ticket.setBirthdate(body.getBirthdate());
        if (body.getDisabled() != null)
            ticket.setDisabled((body.getDisabled()));
        else
            ticket.setDisabled(false);

        if (body.getDiscountCard() != null)
            ticket.setDiscountCard((body.getDiscountCard()));
        else
            ticket.setDiscountCard(null);

        if (body.getStudent() != null)
            ticket.setStudent((body.getStudent()));
        else
            ticket.setStudent(false);


        if (body.getZone() != null) {
            try {
                ticket.setZone(Ticket.ZoneEnum.fromValue(body.getZone().toString()));
            } catch (IllegalArgumentException e) {
                return new ResponseEntity<>(HttpStatus.valueOf(400));
            }
        } else
            ticket.setZone(null);

        LocalDate birthdate = LocalDate.parse(ticket.getBirthdate(), DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate curDate = LocalDate.now();
        int age = Period.between(birthdate, curDate).getYears();
        boolean isSenior = age >= 60;
        if (age >= 28 && ticket.getStudent()) {
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }
        boolean isChild = age < 14;

        if (age < 0)
            return new ResponseEntity<>(HttpStatus.valueOf(400));

        switch(ticket.getValidFor()) {
            case _1H:
                if (ticket.getZone() == null || ticket.getStudent() || isSenior || ticket.getDiscountCard() != null) {
                    return new ResponseEntity<>(HttpStatus.valueOf(400));
                } break;
            case _1D:
                if (ticket.getZone() != null || isChild || ticket.getStudent() || ticket.getDiscountCard() != null) {
                    return new ResponseEntity<>(HttpStatus.valueOf(400));
                } break;
            case _30D: case _1Y:
                if (ticket.getZone() != null) {
                    return new ResponseEntity<>(HttpStatus.valueOf(400));
                } break;
            default:
                return new ResponseEntity<>(HttpStatus.valueOf(400));
        }

        // TODO: write reusable method
        Long ticketId;
        while (true) {
            boolean skip = false;
            ticketId = Math.abs(new Random().nextLong());

            for (Long key : Application.tickets.keySet()) {
                if (Objects.equals(ticketId, key)) {
                    skip = true;
                    break;
                }
            }
            if (skip) continue;
            break;
        }
        ticket.setId(ticketId);
        Application.tickets.put(ticketId, ticket);
        return new ResponseEntity<Ticket>(ticket, HttpStatus.valueOf(201));
    }


    @ApiOperation(
            value = "Check if a ticket is valid",
            nickname = "validateTicket",
            notes = "Check if a ticket is valid given the required information to do a validation. " +
                    "Should be invalid if one of the ticket properties doesn't match the provided properties. " +
                    "Zone B includes Zone A. Furthermore Zone C includes all Zones.",
            tags = {"ticket validation"}
    )
    @ApiResponses({@ApiResponse(
            code = 200,
            message = "Ticket is valid"
    ), @ApiResponse(
            code = 403,
            message = "Ticket is not valid"
    )})
    @RequestMapping(
            method = {RequestMethod.POST},
            value = {"/tickets/validate"},
            consumes = {"application/json"}
    )
    public ResponseEntity<Void> validateTicket(@ApiParam(value = "TicketValidationRequest object that needs to validated",required = true)
                                                   @RequestBody @Valid de.rwth.swc.sqa.model.TicketValidationRequest body) {

        if (body.getTicketId() == null)
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        Ticket ticket = Application.tickets.get(body.getTicketId());
        if (ticket == null)
            return new ResponseEntity<>(HttpStatus.valueOf(403));

        //TODO: check if the specification changes
        if (body.getZone() == null)
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        if (ticket.getZone() != null)
            if (body.getZone() == TicketValidationRequest.ZoneEnum.B && ticket.getZone() == Ticket.ZoneEnum.A ||
                    body.getZone() == TicketValidationRequest.ZoneEnum.C && ticket.getZone() == Ticket.ZoneEnum.A ||
                        body.getZone() == TicketValidationRequest.ZoneEnum.C && ticket.getZone() == Ticket.ZoneEnum.B)
                return new ResponseEntity<>(HttpStatus.valueOf(403));

        try {
            LocalDateTime dateFrom = getDateTime(ticket.getValidFrom());
            LocalDateTime dateTo = getDateTime(ticket.getValidFrom());
            switch (ticket.getValidFor()) {
                case _1H:
                    dateTo = dateTo.plusHours(1);
                    if ((getDateTime(body.getDate()).isBefore(dateFrom) || getDateTime(body.getDate()).isAfter(dateTo)) || getDateTime(body.getDate()).isEqual(dateTo))
                        return new ResponseEntity<>(HttpStatus.valueOf(403));
                    break;
                case _1D:
                    dateTo = dateTo.plusDays(1);
                    if ((getDateTime(body.getDate()).toLocalDate().isBefore(dateFrom.toLocalDate())
                            || getDateTime(body.getDate()).toLocalDate().isAfter(dateTo.toLocalDate()))
                            || getDateTime(body.getDate()).toLocalDate().isEqual(dateTo.toLocalDate()))
                        return new ResponseEntity<>(HttpStatus.valueOf(403));
                    break;
                case _30D:
                    dateTo = dateTo.plusDays(30);
                    if ((getDateTime(body.getDate()).toLocalDate().isBefore(dateFrom.toLocalDate())
                            || getDateTime(body.getDate()).toLocalDate().isAfter(dateTo.toLocalDate()))
                            || getDateTime(body.getDate()).toLocalDate().isEqual(dateTo.toLocalDate()))
                        return new ResponseEntity<>(HttpStatus.valueOf(403));
                    break;
                case _1Y:
                    dateTo = dateTo.plusYears(1);
                    if ((getDateTime(body.getDate()).toLocalDate().isBefore(dateFrom.toLocalDate())
                            || getDateTime(body.getDate()).toLocalDate().isAfter(dateTo.toLocalDate()))
                            || getDateTime(body.getDate()).toLocalDate().isEqual(dateTo.toLocalDate()))
                        return new ResponseEntity<>(HttpStatus.valueOf(403));
                    break;
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.valueOf(400));
        }

        Boolean disabled = body.getDisabled();
        Boolean reqIsDisabled = ticket.getDisabled();
        if (disabled == null)
            disabled = false;
        if (!disabled && reqIsDisabled)
            return new ResponseEntity<>(HttpStatus.valueOf(403));

        Boolean isStudent = body.getStudent();
        Boolean reqIsStudent = ticket.getStudent();
        if (isStudent == null)
            isStudent = false;
        if (reqIsStudent == null)
            reqIsStudent = false;
        if (isStudent && !reqIsStudent)
            return new ResponseEntity<>(HttpStatus.valueOf(403));


        if (ticket.getDiscountCard() != null && body.getDiscountCardId() == null)
            return new ResponseEntity<>(HttpStatus.valueOf(403));
        if (ticket.getDiscountCard() != null) {
            de.rwth.swc.sqa.model.DiscountCard discountCard = Application.discountCardTable.get(body.getDiscountCardId());
            if (discountCard == null)
                return new ResponseEntity<>(HttpStatus.valueOf(403));
            LocalDate dcDateFrom = getDate(discountCard.getValidFrom());
            LocalDate dcDateTo = getDate(discountCard.getValidFrom());
            switch (discountCard.getValidFor()) {
                case _30D:
                    dcDateTo = dcDateTo.plusDays(30);
                    break;
                case _1Y:
                    dcDateTo = dcDateTo.plusYears(1);
                    break;
            }
            LocalDateTime date = getDateTime(body.getDate());
            if ((date.toLocalDate().isBefore(dcDateFrom) || date.toLocalDate().isAfter(dcDateTo)))
                return new ResponseEntity<>(HttpStatus.valueOf(403));
        }

        return new ResponseEntity<>(HttpStatus.valueOf(200));
    }
}
