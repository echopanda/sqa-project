package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.Application;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.tomcat.util.descriptor.web.ApplicationParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import javax.validation.constraints.Null;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class CustomerController implements de.rwth.swc.sqa.api.CustomersApi {

    public static LocalDate getDateTime(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public Boolean isValidDate(String date) {
        try {
            LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    //TODO: check if used dates are valid!!!

    public ResponseEntity<de.rwth.swc.sqa.model.Customer> addCustomer(de.rwth.swc.sqa.model.Customer body) {
        if (body.getId() == null) {
            while (true) {
                boolean skip = false;
                body.setId(Math.abs(new Random().nextLong()));

                for (Long key : Application.customerTable.keySet()) {
                    if (Objects.equals(body.getId(), key)) {
                        skip = true;
                        break;
                    }
                }
                if (skip) continue;
                break;
            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (body.getDisabled() == null) {
            body.setDisabled(false);
        }

        boolean birthdateValid = isValidDate(body.getBirthdate());
        if (!birthdateValid)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Application.customerTable.put(body.getId(), body);
        return new ResponseEntity<Customer>(body, HttpStatus.CREATED);
    }

    public ResponseEntity<de.rwth.swc.sqa.model.DiscountCard> addDiscountCardToCustomer(Long customerId, de.rwth.swc.sqa.model.DiscountCard body) {
        {
            if (body.getId() != null) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            body.setId(Math.abs(new Random().nextLong()));
        }

        if (!body.getCustomerId().equals(customerId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (!isValidDate(body.getValidFrom()) ||
                (!body.getType().equals(25) && !body.getType().equals(50))) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        boolean isValidForIsCorrect = false;
        for (DiscountCard.ValidForEnum v : DiscountCard.ValidForEnum.values()) {
            if(body.getValidFor().equals(v)) {
                isValidForIsCorrect = true;
            }
        }
        if (!isValidForIsCorrect)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        {
            boolean found = false;
            for (Long key : Application.customerTable.keySet()) {
                if (Objects.equals(body.getCustomerId(), key)) {
                    found = true;
                    break;
                }
            }
            if (!found) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }


        {
            LocalDate dateFrom = getDateTime(body.getValidFrom());

            LocalDate dateTill = getDateTime(body.getValidFrom());
            if (body.getValidFor() == DiscountCard.ValidForEnum._30D) {
                dateTill = dateTill.plusDays(30);
            }
            if (body.getValidFor() == DiscountCard.ValidForEnum._1Y) {
                dateTill = dateTill.plusYears(1);
            }

            for (Long key : Application.discountCardTable.keySet()) {
                if (body.getCustomerId().equals(Application.discountCardTable.get(key).getCustomerId())) {

                    DiscountCard dc = Application.discountCardTable.get(key);
                    LocalDate _dateFrom_existing = getDateTime(Application.discountCardTable.get(key).getValidFrom());
                    LocalDate _dateTill_existing = getDateTime(Application.discountCardTable.get(key).getValidFrom());
                    if (dc.getValidFor() == DiscountCard.ValidForEnum._30D) {
                        _dateTill_existing = _dateTill_existing.plusDays(30);
                    }
                    if (dc.getValidFor() == DiscountCard.ValidForEnum._1Y) {
                        _dateTill_existing = _dateTill_existing.plusYears(1);
                    }

                    if (
                            (dateFrom.isAfter(_dateFrom_existing) && dateFrom.isBefore(_dateTill_existing))
                                    || (dateTill.isAfter(_dateFrom_existing) && dateTill.isBefore(_dateTill_existing))
                                    || (dateFrom.isBefore(_dateFrom_existing) && dateTill.isAfter(_dateTill_existing))
                                    || dateFrom.isEqual(_dateFrom_existing) || dateTill.isEqual(_dateTill_existing)) {
                        return new ResponseEntity<>(HttpStatus.CONFLICT);
                    }
                }
            }
        }
        Application.discountCardTable.put(body.getId(), body);
        return new ResponseEntity<DiscountCard>(body, HttpStatus.CREATED);
    }

    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        if (customerId == null) {
            return new ResponseEntity<>(HttpStatus.valueOf((400)));
        }

        List<DiscountCard> discountCardsForCustomer = Application.discountCardTable.values().stream()
                .filter(discountCard -> discountCard.getCustomerId().equals(customerId))
                .collect(Collectors.toList());

        if (!discountCardsForCustomer.isEmpty()) {
            return new ResponseEntity<>(discountCardsForCustomer, HttpStatus.valueOf(200));
        } else {
            return new ResponseEntity<>(HttpStatus.valueOf(404));
        }
    }

}
